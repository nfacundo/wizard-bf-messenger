'use strict';
const {Messenger,
    Button,
    Element,
    Image,
    Video,
    GenericTemplate,
    GreetingText,
    PersistentMenuItem,
    PersistentMenu,
    QuickReply,
    QuickReplies,
    ReceiptTemplate,
    ListTemplate,
    Address,
    Summary,
    Adjustment,} = require('fbmessenger')

    const {dotenv} =require ('dotenv');
    const {crypto} =require ('crypto');
// Imports dependencies and set up http server


const
  express = require('express'),
  bodyParser = require('body-parser'),
  app = express().use(bodyParser.json()); // creates express http server

  const initBot = async () => {
    try {
      messenger.setDomainWhitelist(WHITELISTED_DOMAINS);
  
      // Greeting Text
      const greetingText = new GreetingText({ text: 'Welcome to the bot demo.' });
      const greetingTextResult = await messenger.setGreetingText(greetingText);
      console.log(`Greeting Text: ${JSON.stringify(greetingTextResult)}`);
  
      // Get Started Button
      const getStartedResult = await messenger.setGetStarted('START');
      console.log(`Get Started: ${JSON.stringify(getStartedResult)}`);
  
      // Persistent menu
      const menuFAQ = new PersistentMenuItem({
        type: 'web_url',
        title: 'FAQS',
        url: 'https://developers.facebook.com/docs/messenger-platform/faq',
      });
  
      const menuReference = new PersistentMenuItem({
        type: 'web_url',
        title: 'Reference',
        url: 'https://developers.facebook.com/docs/messenger-platform/reference',
      });
  
      const menuHelp = new PersistentMenuItem({
        title: 'Help',
        type: 'nested',
        call_to_actions: [
          menuFAQ,
          menuReference,
        ],
      });
  
      const menuDocs = new PersistentMenuItem({
        type: 'web_url',
        title: 'Messenger Docs',
        url: 'https://developers.facebook.com/docs/messenger-platform',
      });
  
      const menu = new PersistentMenu({
        locale: 'default',
        call_to_actions: [menuHelp, menuDocs],
      });
      const persistentMenuResult = await messenger.setPersistentMenu(menu);
      console.log(`PersistentMenu: ${JSON.stringify(persistentMenuResult)}`);
    } catch (e) {
      console.log(e);
    }
  };


  
// Sets server port and logs message on success
app.listen(process.env.PORT || 1337, () => console.log('webhook is listening'));

const messenger = new Messenger({
    pageAccessToken: 'EAAFx6Ox242sBAMmoh3OFDNA11PyMpRCLPviODLWYa4wlspuIIBAth0L8q4QI53pRlwZBieM5w1sphIrC75LbzkhPxOgvhsN7poQuIQe8BaN3RIH58226un77tNCopjCqetSJefP6GcZBtbAEgDxZC2hH67Qgjg305FMpqcaIAZDZD'
})

const getButton = ratio => new Button({
    type: 'web_url',
    title: 'Stack Overflow',
    url: 'https://stackoverflow.com',
    webview_height_ratio: ratio,
  });
  
  const getElement = btn => new Element({
    title: 'Template example',
    item_url: 'https://stackoverflow.com',
    image_url: 'http://placehold.it/300x300',
    subtitle: 'Subtitle',
    buttons: [btn],
  });
  

messenger.on('message', async (message) => {
    console.log(`Message received: ${JSON.stringify(message, true)}`);
    const recipient = message.sender.id;
  
    // Allow receiving locations
    if ('attachments' in message.message) {
      const msgType = message.message.attachments[0].type;
      if (msgType === 'location') {
        console.log('Location received');
        const text = `${message.message.attachments[0].title}:
                      lat: ${message.message.attachments[0].payload.coordinates.lat},
                      long: ${message.message.attachments[0].payload.coordinates.long}`;
        await messenger.send({ text }, recipient);
      }
  
      if (['audio', 'video', 'image', 'file'].includes(msgType)) {
        const attachment = message.message.attachments[0].payload.url;
        console.log(`Attachment received: ${attachment}`);
      }
    }
  
    // Text messages
    if ('text' in message.message) {
      let msg = message.message.text;
      msg = msg.toLowerCase();
  
      if (msg.includes('texto')) {
        await messenger.send({ text: 'Este es un texto de ejemplo.' }, recipient);
      }

      if (msg.includes('hola')) {
        await messenger.send({ text: 'Hola, sea bienvenido a nuestra página, ¿qué desea?.' }, recipient);
      }
      else if (msg.includes('Hola')) {
        await messenger.send({ text: 'Hola, sea bienvenido a nuestra página, ¿qué desea?.' }, recipient);
      }
  
      if (msg.includes('imagen')) {
        await messenger.send({ text: 'Imagen de ejemplo, cargando...' }, recipient);
        const res = await messenger.send(new Image({
          url: 'https://unsplash.it/300/200/?random',
          is_reusable: true,
        }), recipient);
        console.log(`Resuable attachment ID: ${res.attachment_id}`);
      }
  
      if (msg.includes('reuse')) {
        await messenger.send(new Image({ attachment_id: 947782652018100 }), recipient);
      }
  
      if (msg.includes('video')) {
        try {
            await messenger.send({ text: 'Enviando video de ejemplo, espere...' }, recipient);
          await messenger.send(new Video({
            url: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4',
          }), recipient);
        } catch (e) {
          console.error(e);
        }
      }
  
      if (msg.includes('payload')) {
        const pl = message.message.quick_reply.payload;
        const text = `Botón clickeado por usuario: ${msg}, El botón payload es: ${pl}`;
        await messenger.send({ text }, recipient);
      }
  
      if (msg.includes('burbuja')) {
        const element = new Element({
          title: 'Ejemplo 1',
          item_url: 'http://www.bbc.co.uk',
          image_url: 'https://unsplash.it/300/200/?random',
          subtitle: 'Abre bbc.co.uk',
          buttons: [
            new Button({
              type: 'web_url',
              title: 'BBC',
              url: 'http://www.bbc.co.uk',
            }),
          ],
        });
        await messenger.send(new GenericTemplate({
          elements: [element],
        }), recipient);
      }
  
      if (msg.includes('multiple')) {
        await messenger.send({ text: 'Mensaje 1' }, recipient);
        await timeout(3000);
        await messenger.send({ text: 'Mensaje 2' }, recipient);
      }
  
      if (msg.includes('recibo')) {
        const template = new ReceiptTemplate({
          recipient_name: 'Nombre',
          order_number: '123',
          currency: 'COP',
          payment_method: 'Visa',
          order_url: 'http://www.ejemplo.com',
          timestamp: '123123123',
          elements: [
            new Element({
              title: 'Título',
              subtitle: 'Subtitulo',
              quantity: 1,
              price: 12.500,
              currency: 'COP',
            }),
          ],
          address: new Address({
            street_1: 'Calle falsa 123',
            street_2: '',
            city: 'Parque Principal',
            postal_code: '00000',
            state: 'BOG',
            country: 'COL',
          }),
          summary: new Summary({
            subtotal: 75000,
            shipping_cost: 4900,
            total_tax: 6100,
            total_cost: 56100,
          }),
          adjustments: [
            new Adjustment({
              name: 'Adjustment',
              amount: 1900,
            }),
          ],
        });
        const res = await messenger.send(template, recipient);
        console.log(res);
      }
    }
  });

  messenger.on('delivery', () => {
    // console.log(messenger.lastEntry);
  });
  
  messenger.on('postback', (message) => {
    const recipient = message.sender.id;
    const { payload } = message.postback;
    console.log(`Payload received: ${payload}`);
  
    if (payload === 'help') {
      messenger.send({ text: 'Puede ir aquí un mensaje de ejemplo.' }, recipient);
    } else if (payload === 'START') {
      const text = `Trata de mandar un mensaje con estas palabras clave:
  texto, imagen, video, burbuja, recibo o multiple`;
      messenger.send({ text }, recipient);
    }
  });

app.post('/webhook', (req, res) => {
    res.sendStatus(200);
    messenger.handle(req.body)
  });

app.get('/webhook', (req, res) => {
  let VERIFY_TOKEN = "x"
  let mode = req.query['hub.mode'];
  let token = req.query['hub.verify_token'];
  let challenge = req.query['hub.challenge'];
    
  if (mode && token) {
  
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {
      res.status(200).send(challenge);
    
    } else {
      res.sendStatus(403);      
    }
  }

  app.get('/init', async (req, res) => {
    await initBot();
    res.sendStatus(200);
  });
});